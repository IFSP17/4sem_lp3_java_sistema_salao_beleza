-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema salaodebeleza
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema salaodebeleza
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `salaodebeleza` DEFAULT CHARACTER SET utf8 ;
USE `salaodebeleza` ;

-- -----------------------------------------------------
-- Table `salaodebeleza`.`cliente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `salaodebeleza`.`cliente` (
  `cpf_cliente` VARCHAR(20) NOT NULL,
  `nome_cliente` VARCHAR(60) NOT NULL,
  `dia_nasc_cliente` INT NOT NULL,
  `mes_nasc_cliente` INT NOT NULL,
  `ano_nasc_cliente` INT NOT NULL,
  `rg_cliente` VARCHAR(20) NOT NULL,
  `telefone_cliente` VARCHAR(30) NOT NULL,
  `celular_cliente` VARCHAR(30) NULL DEFAULT NULL,
  `endereco_cliente` VARCHAR(100) NOT NULL,
  `status_cliente` VARCHAR(10) NOT NULL,
  `bairro_cliente` VARCHAR(20) NOT NULL,
  `num_residencial_cliente` VARCHAR(8) NOT NULL,
  `cidade_cliente` VARCHAR(80) NOT NULL,
  `sexo_cliente` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`cpf_cliente`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `salaodebeleza`.`funcionario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `salaodebeleza`.`funcionario` (
  `cpf_funcionario` VARCHAR(20) NOT NULL,
  `nome_funcionario` VARCHAR(80) NOT NULL,
  `dia_nasc_funcionario` INT NOT NULL,
  `mes_nasc_funcionario` INT NOT NULL,
  `ano_nasc_funcionario` INT NOT NULL,
  `rg_funcionario` VARCHAR(20) NOT NULL,
  `telefone_funcionario` VARCHAR(30) NOT NULL,
  `celular_funcionario` VARCHAR(30) NULL DEFAULT NULL,
  `endereco_funcionario` VARCHAR(60) NOT NULL,
  `bairro_funcionario` VARCHAR(30) NOT NULL,
  `num_residencial_funcionario` VARCHAR(8) NOT NULL,
  `cidade_funcionario` VARCHAR(50) NOT NULL,
  `cargo_funcionario` VARCHAR(30) NOT NULL,
  `salario_funcionario` DOUBLE NOT NULL,
  `senha_funcionario` VARCHAR(20) NOT NULL,
  `sexo_funcionario` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`cpf_funcionario`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `salaodebeleza`.`servico`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `salaodebeleza`.`servico` (
  `codigo_servico` INT(11) NOT NULL AUTO_INCREMENT,
  `nome_servico` VARCHAR(60) NOT NULL,
  `preco_servico` DOUBLE NOT NULL,
  `duracao_servico` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`codigo_servico`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `salaodebeleza`.`servico_funcionario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `salaodebeleza`.`servico_funcionario` (
  `codigo_servico` INT(11) NOT NULL,
  `cpf_funcionario` VARCHAR(20) NOT NULL,
  `codigo_servico_funcionario` INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`codigo_servico_funcionario`),
  INDEX `fk_servico_has_funcionario_funcionario1_idx` (`cpf_funcionario` ASC),
  INDEX `fk_servico_has_funcionario_servico1_idx` (`codigo_servico` ASC),
  CONSTRAINT `fk_servico_has_funcionario_funcionario1`
    FOREIGN KEY (`cpf_funcionario`)
    REFERENCES `salaodebeleza`.`funcionario` (`cpf_funcionario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_servico_has_funcionario_servico1`
    FOREIGN KEY (`codigo_servico`)
    REFERENCES `salaodebeleza`.`servico` (`codigo_servico`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `salaodebeleza`.`agenda`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `salaodebeleza`.`agenda` (
  `codigo_agenda` INT NOT NULL AUTO_INCREMENT,
  `cpf_cliente` VARCHAR(20) NOT NULL,
  `dia_agenda` INT NOT NULL,
  `mes_agenda` INT NOT NULL,
  `ano_agenda` INT NOT NULL,
  `horario_agenda` VARCHAR(20) NOT NULL,
  `codigo_servico_funcionario` INT NOT NULL,
  `situacao_agenda` VARCHAR(45) NOT NULL,
  INDEX `fk_cliente_has_servico_cliente_idx` (`cpf_cliente` ASC),
  PRIMARY KEY (`codigo_agenda`),
  INDEX `fk_agenda_servico_funcionario1_idx` (`codigo_servico_funcionario` ASC),
  CONSTRAINT `fk_cliente_has_servico_cliente`
    FOREIGN KEY (`cpf_cliente`)
    REFERENCES `salaodebeleza`.`cliente` (`cpf_cliente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_agenda_servico_funcionario1`
    FOREIGN KEY (`codigo_servico_funcionario`)
    REFERENCES `salaodebeleza`.`servico_funcionario` (`codigo_servico_funcionario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `salaodebeleza`.`produto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `salaodebeleza`.`produto` (
  `codigo_produto` INT(11) NOT NULL AUTO_INCREMENT,
  `nome_produto` VARCHAR(30) NOT NULL,
  `categoria_produto` VARCHAR(20) NOT NULL,
  `tamanho_produto` DOUBLE NOT NULL,
  `unidade_medida_produto` VARCHAR(8) NOT NULL,
  `preco_produto` DOUBLE NOT NULL,
  `fornecedor_produto` VARCHAR(40) NOT NULL,
  PRIMARY KEY (`codigo_produto`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `salaodebeleza`.`aquisicao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `salaodebeleza`.`aquisicao` (
  `codigo_aquisicao` INT(11) NOT NULL AUTO_INCREMENT,
  `codigo_produto` INT(11) NOT NULL,
  `dia_aquisicao` INT NOT NULL,
  `mes_aquisicao` INT NOT NULL,
  `ano_aquisicao` INT NOT NULL,
  `dia_confirmacao_aquisicao` INT NULL,
  `mes_confirmacao_aquisicao` INT NULL,
  `ano_confirmacao_aquisicao` INT NULL,
  `dia_entrega_aquisicao` INT NULL,
  `mes_entrega_aquisicao` INT NULL,
  `ano_entrega_aquisicao` INT NULL,
  `dia_prazo_pagamento_aquisicao` INT NULL,
  `mes_prazo_pagamento_aquisicao` INT NULL,
  `ano_prazo_pagamento_aquisicao` INT NULL,
  `status_aquisicao` VARCHAR(20) NOT NULL,
  `quantidade_produto_aquisicao` INT NOT NULL,
  `valor_total_aquisicao` DOUBLE NOT NULL,
  `cpf_funcionario` VARCHAR(20) NULL,
  PRIMARY KEY (`codigo_aquisicao`),
  INDEX `fk_aquisicao_produto1_idx` (`codigo_produto` ASC),
  INDEX `fk_aquisicao_funcionario1_idx` (`cpf_funcionario` ASC),
  CONSTRAINT `fk_aquisicao_produto1`
    FOREIGN KEY (`codigo_produto`)
    REFERENCES `salaodebeleza`.`produto` (`codigo_produto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_aquisicao_funcionario1`
    FOREIGN KEY (`cpf_funcionario`)
    REFERENCES `salaodebeleza`.`funcionario` (`cpf_funcionario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `salaodebeleza`.`estoque`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `salaodebeleza`.`estoque` (
  `codigo_estoque` INT(11) NOT NULL AUTO_INCREMENT,
  `codigo_produto` INT(11) NOT NULL,
  `quantidade_minima_estoque` INT(11) NOT NULL,
  `quantidade_recomendada_estoque` INT(11) NOT NULL,
  `quantidade_atual_estoque` INT(11) NOT NULL,
  PRIMARY KEY (`codigo_estoque`),
  INDEX `fk_estoque_produto1_idx` (`codigo_produto` ASC),
  CONSTRAINT `fk_estoque_produto1`
    FOREIGN KEY (`codigo_produto`)
    REFERENCES `salaodebeleza`.`produto` (`codigo_produto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `salaodebeleza`.`pagamento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `salaodebeleza`.`pagamento` (
  `codigo_pagamento` INT(11) NOT NULL AUTO_INCREMENT,
  `situacao_pagamento` VARCHAR(20) NOT NULL,
  `dia_pagamento` INT NOT NULL,
  `mes_pagamento` INT NOT NULL,
  `ano_pagamento` INT NOT NULL,
  `num_parcela_pagamento` INT(11) NULL,
  `valor_parcela_pagamento` DOUBLE NULL,
  `tipo_pagamento` VARCHAR(40) NOT NULL,
  `valor_total_pagamento` DOUBLE NOT NULL,
  `prazo_parcela_pagamento` VARCHAR(20) NULL,
  `parcela_paga_pagamento` INT NULL,
  PRIMARY KEY (`codigo_pagamento`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `salaodebeleza`.`promocao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `salaodebeleza`.`promocao` (
  `codigo_promocao` INT(11) NOT NULL AUTO_INCREMENT,
  `codigo_servico` INT(11) NOT NULL,
  `nome_promocao` VARCHAR(30) NOT NULL,
  `preco_promocao` DOUBLE NOT NULL,
  `status_promocao` VARCHAR(40) NOT NULL,
  PRIMARY KEY (`codigo_promocao`),
  INDEX `fk_promocao_servico1_idx` (`codigo_servico` ASC),
  CONSTRAINT `fk_promocao_servico1`
    FOREIGN KEY (`codigo_servico`)
    REFERENCES `salaodebeleza`.`servico` (`codigo_servico`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `salaodebeleza`.`pagamento_agenda`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `salaodebeleza`.`pagamento_agenda` (
  `codigo_pagamento` INT(11) NOT NULL,
  `codigo_agenda` INT NOT NULL,
  `codigo_pagamento_agenda` INT NOT NULL AUTO_INCREMENT,
  INDEX `fk_pagamento_has_agenda_agenda1_idx` (`codigo_agenda` ASC),
  INDEX `fk_pagamento_has_agenda_pagamento1_idx` (`codigo_pagamento` ASC),
  PRIMARY KEY (`codigo_pagamento_agenda`),
  CONSTRAINT `fk_pagamento_has_agenda_pagamento1`
    FOREIGN KEY (`codigo_pagamento`)
    REFERENCES `salaodebeleza`.`pagamento` (`codigo_pagamento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pagamento_has_agenda_agenda1`
    FOREIGN KEY (`codigo_agenda`)
    REFERENCES `salaodebeleza`.`agenda` (`codigo_agenda`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

INSERT INTO `salaodebeleza`.`funcionario` (`cpf_funcionario`, `nome_funcionario`, `dia_nasc_funcionario`, `mes_nasc_funcionario`, `ano_nasc_funcionario`, `rg_funcionario`, `telefone_funcionario`, `celular_funcionario`, `endereco_funcionario`, `bairro_funcionario`, `num_residencial_funcionario`, `cidade_funcionario`, `cargo_funcionario`, `salario_funcionario`, `senha_funcionario`, `sexo_funcionario`) VALUES ('123', 'admin', '0', '0', '0', '0', '0', '0', '--', '--', '--', '--', 'Gerente', '0', '123', 'Masculino');


INSERT INTO `salaodebeleza`.`funcionario` (`cpf_funcionario`, `nome_funcionario`, `dia_nasc_funcionario`, `mes_nasc_funcionario`, `ano_nasc_funcionario`, `rg_funcionario`, `telefone_funcionario`, `celular_funcionario`, `endereco_funcionario`, `bairro_funcionario`, `num_residencial_funcionario`, `cidade_funcionario`, `cargo_funcionario`, `salario_funcionario`, `senha_funcionario`, `sexo_funcionario`) VALUES ('456', 'supervisor', '0', '0', '0', '0', '0', '0', '--', '--', '--', '--', 'Supervisor', '0', '456', 'Masculino');

